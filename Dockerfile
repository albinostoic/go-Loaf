FROM golang

WORKDIR /go/src/app

RUN cp -r /usr/local/go/src/cmd/internal /usr/local/go/src/cmd/objfile

RUN go get -d -v github.com/gorilla/websocket \
  github.com/asdine/storm \
  github.com/mgutz/logxi/v1 \
  github.com/bwmarrin/discordgo \
  github.com/dearplain/goloader

RUN go install -v -i github.com/gorilla/websocket \
  github.com/asdine/storm \
  github.com/mgutz/logxi/v1 \
  github.com/bwmarrin/discordgo \
  github.com/dearplain/goloader

WORKDIR /go/src/gitlab.com/albinostoic/go-Loaf
ADD src src/
RUN go install -i gitlab.com/albinostoic/go-Loaf/src && \
    cp $GOPATH/bin/src /go/src/app/loaf

COPY plugins build-plugins.bash plugins/
RUN mkdir -p "/go/src/app/run/" && \
    /bin/bash ./plugins/build-plugins.bash

WORKDIR /go/src/app/run
CMD LOGXI=* ../loaf --lib=/go/src/app
