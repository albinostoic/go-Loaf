#!/usr/bin/env bash

running_in_docker() {
  awk -F/ '$2 == "docker"' /proc/self/cgroup | read
}

if [[ -d "/c" ]]; then
  echo "You could be running this from Windows, don't do that."
  exit 2
fi

if [[ ! running_in_docker ]]; then
  echo "You should not run this script directly, it is used by the docker container."
  exit 2
fi

cd "$GOPATH/src/gitlab.com/albinostoic/go-Loaf/plugins"
for f in *; do
  if [[ -d $f ]]; then
    echo "==> Building Plugin $f ..."
    cd $f
    extras=$(find . -type f -name '*.go' -a ! -name 'main.go')
    go tool compile -I "$GOPATH/pkg/linux_amd64" ./main.go $extras
    cp "main.o" "/go/src/app/$f.so"
    cd ..
  fi
done

echo "==> Done Building Plugins"
