#!/usr/bin/env bash

imagename="loaf"
mountarg="type=bind,src=$(pwd)/bin,dst=/go/src/app/run"

docker build -t "$imagename" .

if [[ -d "/c" ]]; then
  echo "[NOTE] Detected you may be running Windows.  Will use winpty"
  winpty docker run -it --rm --mount "$mountarg" --name goLoaf "$imagename"
else
  docker run -it --rm --mount "$mountarg" --name goLoaf "$imagename"
fi
