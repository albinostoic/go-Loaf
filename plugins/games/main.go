package main

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/albinostoic/go-Loaf/src/bot"
)

func main() {}

func GetVersion() string {
	return "0.2.1.8"
}

var (
	// Games map[string]Game
	Usage string
)

// func RegisterGame(key string, game Game) {
// 	Games[key] = game
// }

func Init(app *bot.App) {
	// Games = make(map[string]Game)
	// RegisterGame("Coin", &Coin{})
	// RegisterGame("Sim", &Sim{})

	Usage = strings.Replace(`Commands:
    {prefix}games
    {prefix}room create <name> <game>
    {prefix}room join <room>
    {prefix}room list [game]
    {prefix}room start`, "{prefix}", app.Config.Bot.Prefix, -1)
}

func help(s *discordgo.Session, channel string, user *discordgo.User) {
	complex(s, "Help", channel, Usage, user)
}

func MessageCreated(app *bot.App, s *discordgo.Session, m *discordgo.MessageCreate) {
	// if Games == nil {
	// 	Init(app)
	// }

	if strings.HasPrefix(strings.ToLower(m.Content), app.Config.Bot.Prefix+"games") {
		go s.ChannelMessageDelete(m.ChannelID, m.ID)

		if m.ChannelID != "435978332899770378" && m.ChannelID != "430208289234747393" {
			s.ChannelMessageSend(m.ChannelID, m.Author.Mention()+", I'm not allowed to game in this channel!")
			return
		}

		// gameNames := make([]string, 0)
		// fmt.Println(fmt.Sprintf("%+v", Games))
		//
		// for k, _ := range Games {
		// 	gameNames = append(gameNames, k)
		// }
		// complex(s, m.ChannelID, "List", strings.Join(gameNames, "\n"), m.Author)
	}

	if strings.HasPrefix(strings.ToLower(m.Content), app.Config.Bot.Prefix+"room") {
		go s.ChannelMessageDelete(m.ChannelID, m.ID)

		if m.ChannelID != "435978332899770378" && m.ChannelID != "430208289234747393" {
			s.ChannelMessageSend(m.ChannelID, m.Author.Mention()+", I'm not allowed to game in this channel!")
			return
		}

		msg := strings.Trim(m.Content[len(app.Config.Bot.Prefix+"room"):], " \r\n")
		if len(msg) == 0 {
			help(s, m.ChannelID, m.Author)
			return
		}
		parts := strings.Fields(msg)
		switch strings.ToLower(parts[0]) {
		case "create":
		case "join":
		case "list":
		case "start":
			if len(parts) == 1 {
				s.ChannelMessageSend(m.ChannelID, m.Author.Mention()+", no game specified.  Please try `games list`")
			}

			// if game, ok := Games[parts[1]]; ok {
			// 	room := game.Join(m.ChannelID, m.Author.ID)
			// 	room.State = &RoomState{
			// 		Channel: m.ChannelID,
			// 		Session: s,
			// 	}
			// 	game.Start(room)
			// }

		default:
		}
	}
}

func complex(s *discordgo.Session, channel, title, description string, u *discordgo.User) {
	s.ChannelMessageSendComplex(channel, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				IconURL: u.AvatarURL("32x32"),
				Name:    u.Username},
			Color:       3447003,
			Title:       fmt.Sprintf("Games > %s", strings.Title(title)),
			Description: description,
		}})
}
