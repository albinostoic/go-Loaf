package main

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/albinostoic/go-Loaf/src/bot"
)

func main() {}

func GetVersion() string {
	return "0.3.1.19"
}

var phrases = []string{
	"thank you",
	"thanks",
	"thx",
}

func MessageCreated(app *bot.App, s *discordgo.Session, m *discordgo.MessageCreate) {
	lower := strings.ToLower(m.Content)
	if strings.HasPrefix(lower, app.Config.Bot.Prefix+"karma") {
		getKarma(app, s, m)
	}

	for _, x := range phrases {
		if strings.Contains(lower, x) {
			setKarma(app, s, m)
			return
		}
	}
}

func setKarma(app *bot.App, s *discordgo.Session, m *discordgo.MessageCreate) {
	// Allows you to query using mentions
	targets := m.Mentions
	if len(targets) == 0 {
		return
	}

	// Buiild a reply based on the target list
	msg := ""
	for _, t := range targets {
		if t.ID == m.Author.ID {
			msg += "Tried to give karma to themselves.\n"
			continue
		}

		err := app.DB.Increment(t.ID, "karma")
		if err == nil {
			u, _ := app.DB.FindOne(t.ID)
			msg += fmt.Sprintf("Gave karma to %s. (total: %d)\n", t.Mention(), u.Karma)
			continue
		}

		msg += t.Username + "Failed to give karma to %s.\n"
		fmt.Println(err.Error())
	}

	if len(msg) > 0 {
		complex(app, s, m.ChannelID, m.Author, msg)
	}
}

func getKarma(app *bot.App, s *discordgo.Session, m *discordgo.MessageCreate) {
	go s.ChannelMessageDelete(m.ChannelID, m.Message.ID)

	// Allows you to query using mentions
	targets := m.Mentions
	if len(targets) == 0 {
		targets = []*discordgo.User{m.Author}
	}

	// Buiild a reply based on the target list
	msg := ""
	for _, t := range targets {
		user, err := app.DB.FindOneOrCreate(t.ID)
		if err == nil && user != nil {
			msg += fmt.Sprintf("%s has %d karma.\n", t.Mention(), user.Karma)
		} else {
			msg += fmt.Sprintf("Error fetching karma for %s.\n", t.Mention())
		}
	}

	if len(msg) > 0 {
		complex(app, s, m.ChannelID, m.Author, msg[:len(msg)-1])
	}
}

func complex(app *bot.App, s *discordgo.Session, channel string, u *discordgo.User, msg string) {
	s.ChannelMessageSendComplex(channel, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				IconURL: u.AvatarURL("32x32"),
				Name:    u.Username},
			Color:       3447003,
			Description: msg,
		}})
}
