package main

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/albinostoic/go-Loaf/src/bot"
)

func main() {}

func GetVersion() string {
	return "0.3.1.25"
}

func MessageCreated(app *bot.App, s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(strings.ToLower(m.Content), app.Config.Bot.Prefix+"whoami") {
		go s.ChannelMessageDelete(m.ChannelID, m.Message.ID)

		whois(app, s, m.Content[len(app.Config.Bot.Prefix):], m.ChannelID, m.Author)
	}

	if strings.HasPrefix(strings.ToLower(m.Content), app.Config.Bot.Prefix+"whois") {
		go s.ChannelMessageDelete(m.ChannelID, m.Message.ID)

		if (len(m.Mentions)) > 0 {
			for _, u := range m.Mentions {
				whois(app, s, m.Content[len(app.Config.Bot.Prefix):], m.ChannelID, u)
				return // only lookup first found user
			}
		}

		for _, f := range strings.Fields(m.Content)[1:] {
			if u, err := s.User(f); err == nil {
				whois(app, s, "whois "+f, m.ChannelID, u)
				return // only lookup first found user
			}
		}
	}
}

func whois(app *bot.App, s *discordgo.Session, command, channel string, u *discordgo.User) {
	word := strings.Split(command, " ")[0]
	word = strings.ToUpper(word[0:1]) + word[1:]

	s.ChannelMessageSendComplex(channel, &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				IconURL: u.AvatarURL("32x32"),
				Name:    u.Username},
			Color:       3447003,
			Title:       "Whoami > " + word,
			Description: fmt.Sprintf("ID: %s", u.ID),
			Footer:      &discordgo.MessageEmbedFooter{Text: command},
		}})
}
