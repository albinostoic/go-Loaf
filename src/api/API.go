package api

type API interface {
	Config() (config Config)
	User(id string) (user *User, err error)
}
