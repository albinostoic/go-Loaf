package api

type Config struct {
	Bot struct {
		AuthToken string
		Prefix    string
	}
	Database struct {
		Filename string
	}
	Plugins []string
}
