package bot

import (
	"gitlab.com/albinostoic/go-Loaf/src/api"
)

type API struct {
	config *api.Config
	db     *Database
}

func (api API) Config() api.Config {
	return *api.config
}

func (api API) User(id string) (*api.User, error) {
	return api.db.FindOneOrCreate(id)
}
