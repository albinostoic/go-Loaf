package bot

import (
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/mgutz/logxi/v1"
	"gitlab.com/albinostoic/go-Loaf/src/api"
)

type App struct {
	API     api.API
	Config  *api.Config
	DB      *Database
	Lib     string
	Logger  log.Logger
	Plugins []*Plugin
}

func (app *App) Run() {
	// Initialize logger
	w := log.NewConcurrentWriter(os.Stdout)
	app.Logger = log.NewLogger(w, "App")

	// Connect to the database
	app.DB = &Database{}
	app.fatalCheck(app.DB.Connect(app.Config.Database.Filename))
	defer app.DB.Close()

	// Connect to Discord
	discord, err := discordgo.New("Bot " + app.Config.Bot.AuthToken)
	app.fatalCheck(err)
	discord = discord

	// Register our global handlers
	discord.AddHandler(app.messageCreated)

	// Setup API
	app.API = &API{
		config: app.Config,
		db:     app.DB}

	// Load all plugins specified in configuration
	app.Plugins = make([]*Plugin, 0)
	app.loadPlugins(discord)

	for _, p := range app.Plugins {
		defer p.Unload()
	}

	// Open Connection
	app.fatalCheck(discord.Open())
	defer discord.Close()

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func (app *App) errorCheck(err error) {
	if err != nil {
		app.Logger.Error("Error", "caller", getCaller(), "error", err)
	}
}

func (app *App) fatalCheck(err error) {
	if err != nil {
		app.Logger.Error("Fatal", "caller", getCaller(), "error", err)
		os.Exit(-1)
	}
}

func getCaller() string {
	fpc := make([]uintptr, 1)
	n := runtime.Callers(3, fpc)
	if n == 0 {
		return "n/a"
	}

	f := runtime.FuncForPC(fpc[0] - 1)
	if f == nil {
		return "n/a"
	}

	return f.Name()
}
