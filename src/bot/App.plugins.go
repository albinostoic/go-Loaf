package bot

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"unsafe"

	"github.com/bwmarrin/discordgo"
	"github.com/dearplain/goloader"

	"gitlab.com/albinostoic/go-Loaf/src/api"
)

func (app *App) RegisterPlugin(discord *discordgo.Session, p *Plugin) {
	// Register each handler the plugin supports
	app.Logger.Info("Registering...", "Plugin", p.Filename, "Version", p.Version)

	if fPtr, ok := p.Syms["main.Entry"]; ok {
		fPtrContainer := (uintptr)(unsafe.Pointer(&fPtr))
		f := *(*func(api.API))(unsafe.Pointer(&fPtrContainer))
		f(app.API)
	} else {
		app.Logger.Warn("Plugin missing Entry(api.API)", "Plugin", p.Filename)
	}

	if fPtr, ok := p.Syms["main.MessageCreated"]; ok {
		fPtrContainer := (uintptr)(unsafe.Pointer(&fPtr))
		f := *(*func(*App, *discordgo.Session, *discordgo.MessageCreate))(unsafe.Pointer(&fPtrContainer))

		app.Logger.Info("Adding Handler", "Plugin", p.Filename, "Handler", "MessageCreated")
		discord.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
			// Ignore own messages
			if m.Author.ID != s.State.User.ID {
				// Call the plugin's handler
				f(app, s, m)
			}
		})
	}

	app.Plugins = append(app.Plugins, p)
}

func (app *App) loadPlugins(discord *discordgo.Session) {
	for _, p := range app.Config.Plugins {
		// Attempt to open a file by that name as a shared library
		f, err := os.Open(filepath.Join(app.Lib, p))
		if err != nil {
			app.errorCheck(err)
			continue
		}
		defer f.Close()

		symPtr := make(map[string]uintptr)
		goloader.RegSymbol(symPtr)
		goloader.RegTypes(symPtr, fmt.Sprintf)
		goloader.RegTypes(symPtr, strings.Contains, strings.Fields, strings.ContainsAny,
			strings.ContainsRune, strings.Count, strings.HasPrefix, strings.HasSuffix,
			strings.Index, strings.IndexAny, strings.IndexByte, strings.IndexRune,
			strings.Join, strings.LastIndex, strings.LastIndexAny, strings.LastIndexByte,
			strings.Map, strings.NewReader, strings.NewReplacer, strings.Repeat,
			strings.Replace, strings.Replacer{}, strings.Split, strings.SplitN,
			strings.SplitAfter, strings.SplitAfterN, strings.Trim, strings.Title,
			strings.ToLower, strings.ToLowerSpecial, strings.ToTitleSpecial,
			strings.ToUpperSpecial, strings.Trim, strings.TrimFunc, strings.TrimLeft)
		goloader.RegTypes(symPtr, &discordgo.Channel{}, &discordgo.Guild{}, &discordgo.Session{})
		goloader.RegTypes(symPtr, &discordgo.Message{}, &discordgo.MessageAck{},
			&discordgo.MessageSend{}, &discordgo.MessageEdit{}, &discordgo.MessageCreate{},
			&discordgo.MessageUpdate{}, &discordgo.MessageDelete{}, &discordgo.MessageReaction{},
			&discordgo.MessageReactions{}, &discordgo.MessageReactionRemove{}, &discordgo.MessageReactionRemoveAll{})
		goloader.RegTypes(symPtr, &discordgo.MessageEmbed{}, &discordgo.MessageEmbedAuthor{},
			&discordgo.MessageEmbedField{}, &discordgo.MessageEmbedFooter{}, &discordgo.MessageEmbedImage{},
			&discordgo.MessageEmbedProvider{}, &discordgo.MessageEmbedThumbnail{}, &discordgo.MessageEmbedVideo{})

		object, err := goloader.ReadObj(f)
		if err != nil {
			app.errorCheck(err)
			continue
		}

		module, err := goloader.Load(object, symPtr)
		if err != nil {
			app.errorCheck(err)
			continue
		}

		var version string
		vPtr, ok := module.Syms["main.GetVersion"]
		if ok {
			vPtrContainer := (uintptr)(unsafe.Pointer(&vPtr))
			v := *(*func() string)(unsafe.Pointer(&vPtrContainer))
			version = v()
		}
		if version == "" {
			app.Logger.Warn("Skipping invalid or outdated Plugin", "Plugin", p)
			app.errorCheck(err)
			continue
		}

		app.RegisterPlugin(discord, &Plugin{
			CodeModule: module,
			Enabled:    true,
			Filename:   p,
			Version:    version})
	}
}

func (app *App) messageCreated(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(strings.ToLower(m.Content), app.Config.Bot.Prefix+"plugins") {
		go s.ChannelMessageDelete(m.ChannelID, m.Message.ID)

		pluginsEnabled := 0
		pluginFields := make([]*discordgo.MessageEmbedField, 0)
		for _, p := range app.Plugins {
			if p.Enabled {
				pluginsEnabled++
				pluginFields = append(pluginFields, &discordgo.MessageEmbedField{
					Inline: true,
					Name:   fmt.Sprintf("%s (%s)", p.Filename, p.Version),
					Value:  "Enabled"})
			} else {
				pluginFields = append(pluginFields, &discordgo.MessageEmbedField{
					Inline: true,
					Name:   fmt.Sprintf("%s (%s)", p.Filename, p.Version),
					Value:  "Disabled"})
			}
		}

		s.ChannelMessageSendComplex(m.ChannelID, &discordgo.MessageSend{
			Embed: &discordgo.MessageEmbed{
				Author: &discordgo.MessageEmbedAuthor{
					IconURL: m.Author.AvatarURL("32x32"),
					Name:    m.Author.Username},
				Color:       3447003,
				Title:       "Core > Plugins",
				Description: fmt.Sprintf("There are %d plugins loaded, %d of which are enabled.", len(app.Plugins), pluginsEnabled),
				Fields:      pluginFields,
			}})
	}
}
