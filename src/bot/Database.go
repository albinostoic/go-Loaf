package bot

import (
	"fmt"
	"strings"

	"github.com/asdine/storm"
	"gitlab.com/albinostoic/go-Loaf/src/api"
)

var (
	tableName string = "users"
)

type Database struct {
	*storm.DB

	filename string
}

func (db *Database) Connect(filename string) error {
	if filename == "" {
		filename = "go-Loaf.db"
	}
	db.filename = filename

	conn, err := storm.Open(filename)
	if err != nil {
		return err
	}

	db.DB = conn
	return db.Init(&api.User{})
}

func (db *Database) FindOne(id string) (*api.User, error) {
	var user api.User
	err := db.One("ID", id, &user)
	if err != nil {
		err = fmt.Errorf("app.Database.FindOne: %s", err)
	}

	return &user, err
}

func (db *Database) FindOneOrCreate(id string) (record *api.User, err error) {
	record, err = db.FindOne(id)

	// Create record if it doesn't exist
	if err != nil || record == nil {
		record = &api.User{ID: id}
		err = db.Save(record)
		if err != nil {
			err = fmt.Errorf("app.Database.FindOneOrCreate: %s", err)
		}
	}

	return record, err
}

func (db *Database) Increment(id, field string) error {
	user, err := db.FindOneOrCreate(id)
	if err != nil {
		return err
	}
	switch strings.ToLower(field) {
	case "karma":
		user.Karma++
	case "loaflove":
		user.Loaflove++
	}
	err = db.Save(user)
	if err != nil {
		err = fmt.Errorf("app.Database.Increment: %s", err)
	}
	return err
}
