package bot

import "github.com/dearplain/goloader"

type Plugin struct {
	*goloader.CodeModule
	Enabled  bool
	Filename string
	Version  string
}
