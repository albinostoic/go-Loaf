package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"

	"gitlab.com/albinostoic/go-Loaf/src/api"
	"gitlab.com/albinostoic/go-Loaf/src/bot"
)

func loadConfig() (result api.Config, err error) {
	b, err := ioutil.ReadFile("config.json")
	if err != nil {
		return result, err
	}
	err = json.Unmarshal(b, &result)
	return result, err
}

func main() {
	config, err := loadConfig()
	if err != nil {
		panic(err.Error())
	}

	lib := flag.String("lib", "", "path to load plugins from")
	flag.Parse()

	app := bot.App{Config: &config, Lib: *lib}
	app.Run()
}
